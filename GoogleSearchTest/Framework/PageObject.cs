﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace GoogleSearchTest.Framework
{   
    public abstract class PageObject
    {
        private IWebDriver driver;

        [FindsBy(How = How.TagName, Using = "BODY")]
        IWebElement Body { get; set; }

        public virtual void WaitForDoneLoading()
        {
            WaitForCondition(() => Body.Displayed, 5000);
        }

        public PageObject(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
            WaitForDoneLoading();
        }

        public void WaitForCondition(Func<bool> condition, int milliseconds)
        {
            var browserWait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromMilliseconds(100));
            int i = 0;

            while(i < milliseconds)
            {
                try
                {
                    browserWait.Until(w => condition());
                    break;
                }
                catch
                {
                    PageFactory.InitElements(driver, this);
                }
                System.Threading.Thread.Sleep(1000);
                i += 1000;
            }
            if(!condition())
            {
                throw new Exception("Condition failed");
            }
        }
    }
}