﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using GoogleSearchTest.PageObjects;

namespace GoogleSearchTest
{
    public class GoogleTests
    {
        IWebDriver driver;
         
        [Test]
        public void GoogleSearch()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Navigate().GoToUrl("http://www.google.ie");
            GoogleSearchPage googleSearchPage = new GoogleSearchPage(driver);

            googleSearchPage.SetSearchText("irish times");
            googleSearchPage.ClickSearchButton();
            Assert.True(googleSearchPage.GetSearchResults().Contains("http://www.irishtimes.com/"));
            TestContext.WriteLine("Search result found");
        }

        [TearDown]
        public void EndTest()
        {
            driver.Quit();
        }
    }
}
