﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using GoogleSearchTest.Framework;
using OpenQA.Selenium;
using NUnit.Framework;

namespace GoogleSearchTest.PageObjects
{
    class GoogleSearchPage : PageObject
    {
        private IWebDriver driver;

        [FindsBy(How = How.Id, Using = "lst-ib")]
        IWebElement Search_Inputbox { get; set; }

        [FindsBy(How = How.Name, Using = "btnG")]
        IWebElement Search_Button { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".rc .r a")]
        IList<IWebElement> SearchResult_List { get; set; }

        public GoogleSearchPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }

        public override void WaitForDoneLoading()
        {
            WaitForCondition(()=> Search_Inputbox.Displayed, 5000);
        }

        public void SetSearchText(string searchText)
        {
            TestContext.WriteLine("Setting the searchText to: {0}", searchText);
            Search_Inputbox.SendKeys(searchText);
        }

        public void ClickSearchButton()
        {
            TestContext.WriteLine("Clicking the search button");
            Search_Button.Click();
        }

        public IList<string> GetSearchResults()
        {
            TestContext.WriteLine("Getting the search results");
            IList<string> searchResults = new List<string>();
            WaitForCondition(() => SearchResult_List.Count > 0, 5000);

            foreach(IWebElement searchResult in SearchResult_List)
            {
                searchResults.Add(searchResult.GetAttribute("href"));
            }
            return searchResults;
        }
    }
}
