**Summary**:
Simple automated test of the Google search page:

 - Enters search text "irish times"
 - Clicks search button
 - Verify link to the irish times website returns in the results

**Dependencies**:
 - Built with Visual Studio 2015 (should be OK for VS 2012+)

**How to run**:
 - Build in Visual Studio and run from the Test Explorer window